=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_78.txt

Mon Apr 04 11:10:08 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.96 X 60.75 (mm)
Route Border Extents  .......... 55.97 X 60.75 (mm)
Actual Board Area  ............. 1,683.6 (mm)
Actual Route Area  ............. 1,684.72 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,367.2 Sq. (mm)  1,173.45 Sq. (mm) 34.85 %

Pins  .......................... 532
Pins per Route Area  ........... 0.32 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 1
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.4, 0.5, 0.7, 1.4

Nets  .......................... 129
Connections  ................... 462
Open Connections  .............. 0
Differential Pairs  ............ 0
Percent Routed  ................ 100.00 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 2,242.11 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 1, 1.4
Vias  .......................... 252
Via Span  Name                   Quantity
   1-4    026VIA                 5
          0.3VIAh0.15            26
          0.3Via0.15h            8
          0.38VIAh0.2            156
          0.38VIAh0.25           57

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 204
    Parts Mounted on Top  ...... 105
        SMD  ................... 84
        Through  ............... 1
        Test Points  ........... 20
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 99
        SMD  ................... 83
        Through  ............... 0
        Test Points  ........... 16
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 1

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 267
    Holes per Board Area  ...... 0.16 Holes/Sq. (mm)
Mounting Holes  ................ 11
