;; V4.1.0
%net
%Prior=1

%page=Schematic1
\AGND\    \C1\-\2\ \M1\-\3\ \TP1\-\1\ 
\LFF_MIC\    \M1\-\2\ \TP2\-\1\ 
\L_MICS\    \C1\-\1\ \M1\-\1\ \TP3\-\1\ 

%Part
\cap_0402\    \C1\ 
\CS7154B\    \M1\ 
\TP0.8x1.2\    \TP1\ \TP2\ \TP3\ 
