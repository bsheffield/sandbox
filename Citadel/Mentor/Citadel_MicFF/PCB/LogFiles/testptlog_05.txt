=======================================================================
PADS Professional Layout - Version 116.0.506.1217
=======================================================================

Job Directory:    C:\pd_repo\pd\_CITADEL\Mentor\Citadel_Right\PCB\


Design Units:(mm)

Test Point Log:   C:\pd_repo\pd\_CITADEL\Mentor\Citadel_Right\PCB\LogFiles\testptlog_05.txt

Fri Dec 09 16:17:29 2016

Net Name        Ref                  X          Y     Side    Location        Operation Result
==============================================================================================
IOP2R_FB_L      TP171                20.97       2.46 Top     Hanger          Delete              
IOP2R_FB_L      TP171                20.97       2.46 Top     Hanger          Delete              
QLINR_R         TP150                  6.5       21.4 Top     Hanger          Delete              
QLINR_R         TP150                  6.5       21.4 Top     Hanger          Delete              
LINR_R          TP149                 4.94       21.4 Top     Hanger          Delete              
LINR_R          TP149                 4.94       21.4 Top     Hanger          Delete              
$6N2552         TP153               -11.65       2.74 Top     Hanger          Delete              
$6N2552         TP153               -11.65       2.74 Top     Hanger          Delete              
MSP_RST         TP154               -11.64       1.41 Top     Hanger          Delete              
MSP_RST         TP154               -11.64       1.41 Top     Hanger          Delete              
$6N2937         TP155               -14.03       4.98 Top     Hanger          Delete              
$6N2937         TP155               -14.03       4.98 Top     Hanger          Delete              
$6N2945         TP156                -12.7       4.97 Top     Hanger          Delete              
$6N2945         TP156                -12.7       4.97 Top     Hanger          Delete              
