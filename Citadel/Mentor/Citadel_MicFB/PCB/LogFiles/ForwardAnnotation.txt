
                               Forward Annotation
                               ------------------

                       11:32 AM Tuesday, January 10, 2017
       Job Name: C:\pd_repo\pd\_CITADEL\Mentor\Citadel_MicFB\PCB\Main.pcb


Version:  01.01.00

     The PDBs listed in the project file will be searched to satisfy the parts
      requirements of the iCDB only for parts not already found in the
      Target PDB.

     The schematic source is a Common Data Base.

     The AllowAlphaRefDes status indicates that reference
      designators containing all alpha characters should be deleted
      and the relevant symbols repackaged.



     Common Data Base has been read

     Target PDB Name: Work\Layout_Temp\PartsDB.pdb

     Number of Part Numbers: 3
          Part Numb: cap_0402 -> Vend Part: cap_0402 
          Part Numb: CS7154B -> Vend Part: CS7154B 
          Part Numb: TP0.8x1.2 -> Vend Part: TP0.8x1.2 

     Number of Part Names: 0

     Number of Part Labels: 0


     Checking for value differences between symbol properties and PartsDB properties

    WARNING: Block Schematic1, Page ANC Mics, Symbol $7I642:
	Symbol / PartsDB property mismatch:
      Resolved PartNumber = cap_0402
      Symbol Property: Description = CAPACITOR
     PartsDB Property: Description = 

    WARNING: Block Schematic1, Page ANC Mics, Symbol $7I642:
	Symbol / PartsDB property mismatch:
      Resolved PartNumber = cap_0402
      Symbol Property: TYPE = CAP
     PartsDB Property: TYPE = Capacitor

     Checking the validity of the packaging of prepackaged schematic
      symbols.  Only the first error in symbols having the same
      Reference Designator will be reported.

     The packaging of all prepackaged schematic symbols is consistent
      with the Parts DataBase data for the cross mapping of
      symbol pin names to Part Number pin numbers.
      Symbols that were not prepackaged will now be packaged correctly.
      
     No errors in Existing Schematic Packaging.

     The Common DataBase has been read and will be packaged.
     Clustering 5 Symbols:
               5  ****
     Clustering is Complete

     Packager Assignments successfully completed



     3 nets were found containing 8 pins
     5 components were found

     Creating a formatted Schematic Netlist (LogFiles\SchematicNetlist.txt)...
     A formatted Schematic Netlist has been created.

     The Logic DataBase has been compiled from the Schematic Design.
      Use Netload to bring the Component Design into sync.

     This Logic Data was Compiled with 2 warnings.
      Erroneous results may occur if not fixed.

                                     NetLoad
                                     -------

                       11:32 AM Tuesday, January 10, 2017
       Job Name: C:\pd_repo\pd\_CITADEL\Mentor\Citadel_MicFB\PCB\Main.pcb


Version:  02.11.12

	Netloading the Layout.  Unused components will be deleted.

	Unconnected pins will be set to net "(Net0)".

	Schematic reference designator changes will be forward annotated.


     Netload completed successfully with 0 warning(s).
     
     Back Annotating...

  Updating Logic Database...

     Version:  99.00.05

     No changes made to Existing Schematic Packaging.


     There is no symbol data to be back annotated to the schematic source.


     The Logic DataBase has been updated and the Common DataBase has
      automatically been brought into sync with the Logic DataBase.
      Please proceed with your design.

     Finished updating the Logic Database.

     Creating a formatted Schematic Netlist (LogFiles\AfterBakAnnoNetlist.txt)...
     A formatted Schematic Netlist has been created.

            Creating a new netlist text file (LogFiles\KeyinNetList.txt)
            from the Logic Database (Work\Layout_Temp\LogicDB.lgc)...
  A new netlist text file has been generated.



                 Beginning Netload on the Layout Design.
           ---------------------------------------------------
	Assigned net AGND to a mounting hole which was originally on net (Net0).

Forward-Annotation on the Layout Design has been successfully completed.

There were 1 reassignments of nets.
There were 0 traces broken back.
There were 0 nets removed from the Layout Design.