
                                DRC - (Final DRC)
                                -----------------

                       11:28 AM Tuesday, January 10, 2017
       Job Name: C:\pd_repo\pd\_CITADEL\Mentor\Citadel_MicFB\PCB\Main.pcb


    ---------
    PROXIMITY
    ---------
    
    Use DRC Window                   : NO


    Disable Same Cell Pad-Pad Checks : NO


    Enable Same Net Pad-Pad Checks   : NO

    
    Layers Specifed To Check         : Layer 1
                                       Layer 2




    Net Class Clearances And Rules
    ------------------------------

        Components Layer 1 TO Components Layer 1
            No Hazards.

        Traces Layer 1 TO Traces Layer 1
            No Hazards.

        Traces Layer 1 TO Part Pads SMD Layer 1
            No Hazards.

        Part Pads SMD Layer 1 TO Part Pads SMD Layer 1
            No Hazards.

        Traces Layer 1 TO Test Pads Top
            No Hazards.

        Test Pads Top TO Part Pads SMD Layer 1
            No Hazards.

        Test Pads Top TO Test Pads Top
            No Hazards.


        Total Hazards Found : 0




    Planes Clearances And Rules
    ---------------------------

        Positive Planes Layer 1 TO Mounting Holes Layer 1
            No Hazards.

        Positive Planes Layer 1 TO Part Pads SMD Layer 1
            No Hazards.

        Positive Planes Layer 1 TO Route Border
            No Hazards.

        Positive Planes Layer 1 TO Traces Layer 1
            No Hazards.

        Positive Planes Layer 1 TO Test Pads Top
            No Hazards.


        Total Hazards Found : 0




    Non-Net Class Element To Element Clearances And Rules
    -----------------------------------------------------

        Test Points Top TO Test Points Top    Clearance: 0.25mm
            No Hazards.

        Route Border TO Test Pads Top    Clearance: 0mm
            No Hazards.

        Route Border TO Traces Layer 1    Clearance: 0mm
            No Hazards.

        Part Pads SMD Layer 1 TO Route Border    Clearance: 0mm
            No Hazards.

        Mounting Holes Layer 1 TO Test Pads Top    Clearance: 0.25mm
            No Hazards.

    >>  Mounting Holes Layer 1 TO Part Pads SMD Layer 1    Clearance: 0.25mm
            Hazards Found : 1

        Mounting Holes Layer 1 TO Traces Layer 1    Clearance: 0.25mm
            No Hazards.

        Board Outline TO Mounting Holes Layer 1    Clearance: 0mm
            No Hazards.

        Board Outline TO Placement Outlines Layer 1    Clearance: 0.25mm
            No Hazards.

        Board Outline TO Test Pads Top    Clearance: 0mm
            No Hazards.

        Board Outline TO Part Pads SMD Layer 1    Clearance: 0mm
            No Hazards.

        Board Outline TO Traces Layer 1    Clearance: 0mm
            No Hazards.

        Board Outline TO Mounting Holes Layer 2    Clearance: 0mm
            No Hazards.


        Total Hazards Found : 1



    Total Proximity Hazards Found : 1




    ----------------------------
    CONNECTIVITY & SPECIAL RULES
    ----------------------------


    Check For Unplaced Parts                      : YES
        No Hazards.


    Check For Missing Parts                       : YES
        No Hazards.


    Check EP Component Hazards                    : YES
        No Hazards.


    Check Trace Hangers                           : YES
        No Hazards.


    Check Trace Loops                             : YES
        No Hazards.


    Check Trace Widths                            : YES
        No Hazards.


    Check Single Point Nets                       : NO


    Check NonPlane Unrouted/Partially Routed Nets : YES
        No Hazards.


    Check Plane Unrouted/Partial Routed Nets      : YES
        No Hazards.


    Check Routed Plane Pins                       : YES
        No Hazards.


    Check Plane Islands                           : YES
        No Hazards.


    Check Dangling Vias/Jumpers                   : YES
        No Hazards.


    Check Unrouted Pins                           : YES
        No Hazards.


    Check Routed Non-Plated Pins                  : YES
        No Hazards.


    Check Minimum Annular Ring                    : NO


    Check For Vias Under SMD Pads                 : YES
        No Hazards.


    Check For Vias Under Top Place Outlines       : YES
        No Hazards.


    Check For Vias Under Bottom Place Outlines    : YES
        No Hazards.


    Check For Missing Conductive Pads             : NO


    Check For Missing Part Soldermask Pads        : NO


    Check For Missing Via Soldermask Pads         : NO


    Check For Missing Part Cover Layer Pads       : NO


    Check For Missing Via Cover Layer Pads        : NO


    Check For Missing Solderpaste Pads            : NO



    Total Connectivity/SpecialRules Hazards Found : 0



    ====================================================================



    Total DRC Hazards Found : 1



                         11:28 AM Tuesday, January 10, 2017