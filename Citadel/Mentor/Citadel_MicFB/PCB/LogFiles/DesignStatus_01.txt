=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_01.txt

Wed Sep 23 16:29:49 2015

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.9 X 60.9 (mm)
Route Border Extents  .......... 57.15 X 63.43 (mm)
Actual Board Area  ............. 1,653.14 (mm)
Actual Route Area  ............. 3,625.02 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,306.28 Sq. (mm) 1,257.65 Sq. (mm) 38.04 %

Pins  .......................... 479
Pins per Route Area  ........... 0.13 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.25, 0.25, 0.3, 0.35, 0.5, 1
    Layer 2 is a Negative Plane Layer with nets
        AGND
        GND
        GND_R
        Trace Widths  .......... 0.1, 0.13, 0.13
    Layer 3 is a Positive Plane Layer with nets
        VBAT
        VBAT_R
        VBUS
        1V8
        1V35
        3V3_USB
        Trace Widths  .......... 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.5, 1.4

Nets  .......................... 123
Connections  ................... 354
Open Connections  .............. 36
Differential Pairs  ............ 0
Percent Routed  ................ 89.83 %

Netline Length  ................ 119.03 (mm)
Netline Manhattan Length  ...... 138.53 (mm)
Total Trace Length  ............ 1,897.92 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.25, 0.25, 0.3, 0.35, 0.5, 1, 1.4
Vias  .......................... 182
Via Span  Name                   Quantity
   1-4    0.3VIAh0.15            14
          026VIA                 12
          0.38VIAh0.2            56
          0.38VIAh0.25           74
   1-2    0.38VIAh0.25           18
          0.3VIAh0.15            8

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 190
    Parts Mounted on Top  ...... 124
        SMD  ................... 81
        Through  ............... 4
        Test Points  ........... 39
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 66
        SMD  ................... 50
        Through  ............... 1
        Test Points  ........... 15
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 176
    Holes per Board Area  ...... 0.11 Holes/Sq. (mm)
Mounting Holes  ................ 11
