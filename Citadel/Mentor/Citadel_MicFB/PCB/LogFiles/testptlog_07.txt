=======================================================================
PADS Professional Layout - Version 116.0.506.1217
=======================================================================

Job Directory:    C:\pd_repo\pd\_CITADEL\Mentor\Citadel_MicFB\PCB\


Design Units:(mm)

Test Point Log:   C:\pd_repo\pd\_CITADEL\Mentor\Citadel_MicFB\PCB\LogFiles\testptlog_07.txt

Thu Jan 05 23:03:33 2017

Net Name        Ref                  X          Y     Side    Location        Operation Result
==============================================================================================
AD_R+           TP165                 -2.9      -9.12 Bottom  Hanger          Delete              
AD_R+           TP165                 -2.9      -9.12 Bottom  Hanger          Delete              
Mic_V+          TP146                -7.42     -14.75 Bottom  Hanger          Delete              
Mic_V+          TP146                -7.42     -14.75 Bottom  Hanger          Delete              
Mic_V+          TP147                -7.64     -17.54 Bottom  Hanger          Delete              
Mic_V+          TP147                -7.64     -17.54 Bottom  Hanger          Delete              
Mic_V+          TP150                -7.61      -19.9 Bottom  Hanger          Delete              
Mic_V+          TP150                -7.61      -19.9 Bottom  Hanger          Delete              
AGND            TP148               -12.35     -10.86 Bottom  Hanger          Delete              
AGND            TP148               -12.35     -10.86 Bottom  Hanger          Delete              
AGND            TP149               -12.36     -13.01 Bottom  Hanger          Delete              
AGND            TP149               -12.36     -13.01 Bottom  Hanger          Delete              
GND             TP49                    17     -14.81 Bottom  Hanger          Delete              
GND             TP49                    17     -14.81 Bottom  Hanger          Delete              
VBAT+           TP157                18.64      -26.7 Bottom  Hanger          Delete              
VBAT+           TP157                18.64      -26.7 Bottom  Hanger          Delete              
VBAT+           TP158                15.97      -24.9 Bottom  Hanger          Delete              
VBAT+           TP158                15.97      -24.9 Bottom  Hanger          Delete              
GND             TP159                16.69     -29.67 Bottom  Hanger          Delete              
GND             TP159                16.69     -29.67 Bottom  Hanger          Delete              
CD_R+           TP161                12.83     -14.17 Bottom  Hanger          Delete              
CD_R+           TP161                12.83     -14.17 Bottom  Hanger          Delete              
CD_R-           TP164                11.44     -10.96 Bottom  Hanger          Delete              
CD_R-           TP164                11.44     -10.96 Bottom  Hanger          Delete              
MFB_RIGHT       TP48                 11.88      -18.9 Bottom  Hanger          Delete              
MFB_RIGHT       TP48                 11.88      -18.9 Bottom  Hanger          Delete              
GND             TP160                14.45     -27.76 Bottom  Hanger          Delete              
GND             TP160                14.45     -27.76 Bottom  Hanger          Delete              
BAT_TS          TP144                12.17     -30.34 Bottom  Hanger          Delete              
BAT_TS          TP144                12.17     -30.34 Bottom  Hanger          Delete              
BAT_TS          TP145                13.89     -32.31 Bottom  Hanger          Delete              
BAT_TS          TP145                13.89     -32.31 Bottom  Hanger          Delete              
CD_R+           TP162                 9.99     -14.14 Bottom  Hanger          Delete              
CD_R+           TP162                 9.99     -14.14 Bottom  Hanger          Delete              
CD_R-           TP163                 8.27     -10.94 Bottom  Hanger          Delete              
CD_R-           TP163                 8.27     -10.94 Bottom  Hanger          Delete              
AD_R-           TP168                -0.54     -12.05 Bottom  Hanger          Delete              
AD_R-           TP168                -0.54     -12.05 Bottom  Hanger          Delete              
AD_R+           TP166                -6.31      -9.07 Bottom  Hanger          Delete              
AD_R+           TP166                -6.31      -9.07 Bottom  Hanger          Delete              
AD_R-           TP167                -4.22     -12.03 Bottom  Hanger          Delete              
AD_R-           TP167                -4.22     -12.03 Bottom  Hanger          Delete              
FB_R            TP155                 -9.2     -12.98 Bottom  Hanger          Delete              
FB_R            TP155                 -9.2     -12.98 Bottom  Hanger          Delete              
FB_R            TP156                -9.23     -10.85 Bottom  Hanger          Delete              
FB_R            TP156                -9.23     -10.85 Bottom  Hanger          Delete              
FF_R            TP153               -10.66     -17.72 Bottom  Hanger          Delete              
FF_R            TP153               -10.66     -17.72 Bottom  Hanger          Delete              
FF_R            TP154               -10.71     -15.62 Bottom  Hanger          Delete              
FF_R            TP154               -10.71     -15.62 Bottom  Hanger          Delete              
BTN_V-_RIGHT    TP16                -15.41     -12.25 Bottom  Hanger          Delete              
BTN_V-_RIGHT    TP16                -15.41     -12.25 Bottom  Hanger          Delete              
AGND            TP151               -13.38     -15.65 Bottom  Hanger          Delete              
AGND            TP151               -13.38     -15.65 Bottom  Hanger          Delete              
AGND            TP152               -13.44     -17.84 Bottom  Hanger          Delete              
AGND            TP152               -13.44     -17.84 Bottom  Hanger          Delete              
GND             TP46                -16.37     -29.58 Bottom  Hanger          Delete              
GND             TP46                -16.37     -29.58 Bottom  Hanger          Delete              
BTN_V+_RIGHT    TP15                -18.37      -13.8 Bottom  Hanger          Delete              
BTN_V+_RIGHT    TP15                -18.37      -13.8 Bottom  Hanger          Delete              
GND             TP47                -20.44     -24.29 Bottom  Hanger          Delete              
GND             TP47                -20.44     -24.29 Bottom  Hanger          Delete              
