				3D Batch DRC
				------------

		  12:45:55 AM Sunday, February 07, 2016

Info: 3D Batch DRC executed on the entire design

BoardEdge:
	Warning: CONN1 at distance of 0mm, Optimal XY is 0.25mm
	Warning: CONN2 at distance of 0.02mm, Optimal Z is 0.13mm
	Warning: R60 at distance of 0.16mm, Optimal XY is 0.25mm

CONN1:
	Warning: BoardEdge at distance of 0mm, Optimal XY is 0.25mm

CONN2:
	Warning: BoardEdge at distance of 0.02mm, Optimal Z is 0.13mm

R12:
	Warning: R13 at distance of 0mm, Optimal XY is 0.25mm

R13:
	Warning: R12 at distance of 0mm, Optimal XY is 0.25mm

R60:
	Warning: BoardEdge at distance of 0.16mm, Optimal XY is 0.25mm


Total DRC Hazards Found : 8


		  12:45:55 AM Sunday, February 07, 2016
