;; V4.1.0
%net
%Prior=1

%page=Schematic1
\AGND\    \C2\-\2\ \M2\-\3\ \TP4\-\1\ 
\LFF_MIC\    \M2\-\2\ \TP5\-\1\ 
\L_MICS\    \C2\-\1\ \M2\-\1\ \TP6\-\1\ 

%Part
\cap_0402\    \C2\ 
\CS7154B\    \M2\ 
\TP0.8x1.2\    \TP4\ \TP5\ \TP6\ 
