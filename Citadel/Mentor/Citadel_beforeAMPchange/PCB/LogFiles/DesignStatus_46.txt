=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_46.txt

Fri Feb 05 01:43:52 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.97 X 61.11 (mm)
Route Border Extents  .......... 55.97 X 61.11 (mm)
Actual Board Area  ............. 1,674.55 (mm)
Actual Route Area  ............. 1,674.55 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,349.1 Sq. (mm)  1,197.69 Sq. (mm) 35.76 %

Pins  .......................... 524
Pins per Route Area  ........... 0.31 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 1, 1.5
    Layer 2 is a signal layer
        Trace Widths  .......... 0.13, 0.2
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.4, 0.5, 0.7, 1.4

Nets  .......................... 129
Connections  ................... 440
Open Connections  .............. 124
Differential Pairs  ............ 0
Percent Routed  ................ 71.82 %

Netline Length  ................ 673.52 (mm)
Netline Manhattan Length  ...... 843.97 (mm)
Total Trace Length  ............ 1,811.96 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 1, 1.4, 1.5
Vias  .......................... 211
Via Span  Name                   Quantity
   1-4    026VIA                 5
          0.3Via0.15h            8
          0.3VIAh0.15            25
          0.38VIAh0.25           60
          0.38VIAh0.2            113

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 200
    Parts Mounted on Top  ...... 113
        SMD  ................... 93
        Through  ............... 1
        Test Points  ........... 19
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 87
        SMD  ................... 72
        Through  ............... 0
        Test Points  ........... 15
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 222
    Holes per Board Area  ...... 0.13 Holes/Sq. (mm)
Mounting Holes  ................ 7
