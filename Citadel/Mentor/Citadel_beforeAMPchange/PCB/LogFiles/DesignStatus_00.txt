=======================================================================
PADS Professional Layout - Version 116.0.506.1217
=======================================================================

Job Directory:        C:\pd_repo\pd\_CITADEL\Mentor\Citadel\PCB\

Design Status Report: C:\pd_repo\pd\_CITADEL\Mentor\Citadel\PCB\LogFiles\DesignStatus_00.txt

Tue Jan 03 14:23:49 2017

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 60.95 X 81.1 (mm)
Route Border Extents  .......... 60.95 X 81.1 (mm)
Actual Board Area  ............. 3,960.7 (mm)
Actual Route Area  ............. 3,960.7 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    7,921.4 Sq. (mm)  1,517.11 Sq. (mm) 19.15 %

Pins  .......................... 2087
Pins per Route Area  ........... 0.53 Pins/Sq. (mm)

Layers  ........................ 6
    Layer 1 is a signal layer
        Trace Widths  .......... 0.08, 0.08, 0.1, 0.1, 0.13, 0.13, 0.2, 0.2, 0.3, 0.4, 0.5, 1.1
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.2, 0.2, 0.3, 0.5
    Layer 4 is a signal layer
        Trace Widths  .......... 0.13, 0.2, 0.2, 0.3
    Layer 5 is a signal layer
        Trace Widths  .......... 0.2, 0.3, 0.5
    Layer 6 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.2, 0.3, 0.4

Nets  .......................... 498
Connections  ................... 1737
Open Connections  .............. 0
Differential Pairs  ............ 0
Percent Routed  ................ 100.00 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 6,685.24 (mm)

Trace Widths Used (mm)  ........ 0.08, 0.08, 0.1, 0.1, 0.13, 0.13, 0.2, 0.2, 0.3, 0.4, 0.5, 1.1
Vias  .......................... 640
Via Span  Name                   Quantity
   1-6    0.25Via0.1h            4
          0.38VIAh0.2            636

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 943
    Parts Mounted on Top  ...... 424
        SMD  ................... 375
        Through  ............... 0
        Test Points  ........... 49
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 519
        SMD  ................... 445
        Through  ............... 5
        Test Points  ........... 69
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 662
    Holes per Board Area  ...... 0.17 Holes/Sq. (mm)
Mounting Holes  ................ 12
