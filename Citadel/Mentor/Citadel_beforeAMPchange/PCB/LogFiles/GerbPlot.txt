 ----------------------------
|    Gerber Plotting Log     |
|          Started           |
|  Wed Dec 28 17:44:18 2016  |
 ----------------------------


Processing SoldermaskTop.gdo
    Number of Apertures     = 53
    Number of Moves         = 1053
    Number of Draws         = 12
    Number of Arcs          = 0
    Number of Flashes       = 1050
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 3
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 3.2956 (IN)
    Total Move Length       = 371.976 (IN)

Processing SoldermaskBottom.gdo
    Number of Apertures     = 31
    Number of Moves         = 1029
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1029
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 437.164 (IN)

Processing SolderPasteTop.gdo
    Number of Apertures     = 49
    Number of Moves         = 1044
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1044
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 362.755 (IN)

Processing SolderPasteBottom.gdo
    Number of Apertures     = 30
    Number of Moves         = 1017
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1017
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 426.467 (IN)

Processing GeneratedSilkscreenTop.gdo
    Number of Apertures     = 5
    Number of Moves         = 3028
    Number of Draws         = 62250
    Number of Arcs          = 33
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 1069
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 293.63 (IN)
    Total Move Length       = 1169.86 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing GeneratedSilkscreenBottom.gdo
    Number of Apertures     = 4
    Number of Moves         = 1461
    Number of Draws         = 25609
    Number of Arcs          = 11
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 431
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 109.066 (IN)
    Total Move Length       = 905.326 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer6Bottom.gdo
    Number of Apertures     = 40
    Number of Moves         = 2851
    Number of Draws         = 4632
    Number of Arcs          = 4404
    Number of Flashes       = 1664
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 36
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 203.883 (IN)
    Total Move Length       = 1568.1 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer5.gdo
    Number of Apertures     = 10
    Number of Moves         = 669
    Number of Draws         = 1141
    Number of Arcs          = 3213
    Number of Flashes       = 645
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 6
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 257.021 (IN)
    Total Move Length       = 486.117 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer4.gdo
    Number of Apertures     = 10
    Number of Moves         = 860
    Number of Draws         = 2057
    Number of Arcs          = 3689
    Number of Flashes       = 645
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 47
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 246.587 (IN)
    Total Move Length       = 612.287 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer3.gdo
    Number of Apertures     = 12
    Number of Moves         = 760
    Number of Draws         = 1615
    Number of Arcs          = 3122
    Number of Flashes       = 645
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 20
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 193.384 (IN)
    Total Move Length       = 560.187 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer2.gdo
    Number of Apertures     = 6
    Number of Moves         = 647
    Number of Draws         = 940
    Number of Arcs          = 2672
    Number of Flashes       = 645
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 2
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 272.073 (IN)
    Total Move Length       = 469.084 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer1Top.gdo
    Number of Apertures     = 66
    Number of Moves         = 3033
    Number of Draws         = 5033
    Number of Arcs          = 5150
    Number of Flashes       = 1685
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 52
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 194.715 (IN)
    Total Move Length       = 1481.48 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing DrillDrawingThrough.gdo
    Number of Apertures     = 1
    Number of Moves         = 2455
    Number of Draws         = 42420
    Number of Arcs          = 0
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 471.987 (IN)
    Total Move Length       = 1208.1 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing BoardOutline.gdo
    Number of Apertures     = 1
    Number of Moves         = 1
    Number of Draws         = 61
    Number of Arcs          = 58
    Number of Flashes       = 0
    Number of Comments      = 4
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 10.2914 (IN)
    Total Move Length       = 1.2271 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

 ---------------------------
|    Gerber Plotting Log     |
|          Finished          |
|  Wed Dec 28 17:44:22 2016  |
 ----------------------------
