
                              SILKSCREEN GENERATOR
                              --------------------

                       12:08 PM Friday, February 24, 2017
          Job Name: C:\pd_repo\pd\_CITADEL\Mentor\Citadel\PCB\Main.pcb


    Silkscreen Side To Process           : BOTH

    Process All Cell Types               : YES

    Top Silkscreen For Selected Cells    : Silkscreen Reference Designator Top
                                         : Silkscreen Part Number Top
                                         : Silkscreen Outline Top

    Bottom Silkscreen For Selected Cells : Silkscreen Reference Designator Bottom
                                         : Silkscreen Part Number Bottom
                                         : Silkscreen Outline Bottom

    Break Silkscreen Using               : Conductive Pads

    Break Graphics Pad Clearance         : NA

    Break Graphics Via Clearance         : NA

    Break Text Pad Clearance             : NA

    Break Text Via Clearance             : NA

    Broken Graphics Line Width           : 0.25mm

    Broken Text Line Width               : 0.25mm
    


    TOP Silkscreen Data Created Successfully (1296 Geometries).


    BOTTOM Silkscreen Data Created Successfully (1228 Geometries).