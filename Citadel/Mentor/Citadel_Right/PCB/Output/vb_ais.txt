$HEADER$
BOARD_TYPE PCB_DESIGN
UNITS MM
$END HEADER$

$PART_SECTION_BEGIN$
TP49                             TP0.8x1.2                        0.00       17.00      -14.81     BOTTOM     YES
SW2                              TS033W1-N35F-03                  108.00     -25.43     -19.56     BOTTOM     YES
SW3                              TS033W1-N35F-03                  144.90     -15.83     -33.71     BOTTOM     YES
SW6                              TS033W1-N35F-03                  124.40     -21.50     -27.88     BOTTOM     YES
TP144                            TP0.8x1.2                        0.00       12.17      -30.34     BOTTOM     YES
TP145                            TP0.8x1.2                        0.00       13.94      -32.42     BOTTOM     YES
TP146                            TP0.8x1.2                        0.00       -7.42      -14.75     BOTTOM     YES
TP147                            TP0.8x1.2                        0.00       -7.64      -17.54     BOTTOM     YES
TP148                            TP0.8x1.2                        0.00       -12.35     -10.86     BOTTOM     YES
TP150                            TP0.8x1.2                        0.00       -7.61      -19.90     BOTTOM     YES
TP149                            TP0.8x1.2                        0.00       -12.36     -13.01     BOTTOM     YES
TP151                            TP0.8x1.2                        0.00       -13.38     -15.65     BOTTOM     YES
TP152                            TP0.8x1.2                        0.00       -13.44     -17.84     BOTTOM     YES
TP153                            TP0.8x1.2                        0.00       -10.66     -17.72     BOTTOM     YES
TP154                            TP0.8x1.2                        0.00       -10.71     -15.62     BOTTOM     YES
TP155                            TP0.8x1.2                        0.00       -9.20      -12.98     BOTTOM     YES
TP156                            TP0.8x1.2                        0.00       -9.23      -10.85     BOTTOM     YES
TP157                            TP1.5                            0.00       18.64      -26.70     BOTTOM     YES
TP158                            TP1.5                            0.00       16.15      -24.68     BOTTOM     YES
TP160                            TP1.5                            180.00     14.45      -27.76     BOTTOM     YES
TP161                            TP1.5                            0.00       8.91       -9.87      BOTTOM     YES
TP159                            TP1.5                            0.00       16.69      -29.67     BOTTOM     YES
TP162                            TP1.5                            180.00     6.07       -9.84      BOTTOM     YES
TP163                            TP1.5                            0.00       10.05      -13.63     BOTTOM     YES
TP164                            TP1.5                            0.00       13.21      -13.65     BOTTOM     YES
TP165                            TP1.5                            0.00       -2.90      -9.12      BOTTOM     YES
TP166                            TP1.5                            0.00       -6.31      -9.07      BOTTOM     YES
TP167                            TP1.5                            0.00       -4.22      -12.03     BOTTOM     YES
TP15                             TP0.8x1.2                        0.00       -16.45     -16.84     BOTTOM     YES
TP168                            TP1.5                            0.00       -0.54      -12.05     BOTTOM     YES
TP16                             TP0.8x1.2                        0.00       -14.22     -21.40     BOTTOM     YES
D2                               ESD_BI_0402                      180.00     -22.78     -16.62     BOTTOM     YES
TP46                             TP0.8x1.2                        0.00       -16.97     -13.41     BOTTOM     YES
D3                               ESD_BI_0402                      180.00     -11.86     -32.54     BOTTOM     YES
TP47                             TP0.8x1.2                        0.00       -20.44     -24.29     BOTTOM     YES
D10                              ESD_BI_0402                      180.00     -17.52     -27.57     BOTTOM     YES
TP48                             TP0.8x1.2                        0.00       11.88      -18.90     BOTTOM     YES
$PART_SECTION_END$
