=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_95.txt

Tue May 24 17:19:50 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.96 X 60.75 (mm)
Route Border Extents  .......... 55.97 X 60.75 (mm)
Actual Board Area  ............. 1,683.6 (mm)
Actual Route Area  ............. 1,684.72 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,367.2 Sq. (mm)  967.68 Sq. (mm)   28.74 %

Pins  .......................... 534
Pins per Route Area  ........... 0.32 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 1
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.4, 0.5, 0.7, 0.9, 1.4

Nets  .......................... 129
Connections  ................... 458
Open Connections  .............. 0
Differential Pairs  ............ 0
Percent Routed  ................ 100.00 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 2,239.21 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.7, 0.9, 1, 1.4
Vias  .......................... 285
Via Span  Name                   Quantity
   1-4    026VIA                 5
          0.3VIAh0.15            25
          0.3Via0.15h            8
          0.38VIAh0.2            193
          0.38VIAh0.25           54

Teardrops....................... 95
    Pad Teardrops............... 95
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 206
    Parts Mounted on Top  ...... 109
        SMD  ................... 88
        Through  ............... 1
        Test Points  ........... 20
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 97
        SMD  ................... 81
        Through  ............... 0
        Test Points  ........... 16
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 300
    Holes per Board Area  ...... 0.18 Holes/Sq. (mm)
Mounting Holes  ................ 11
