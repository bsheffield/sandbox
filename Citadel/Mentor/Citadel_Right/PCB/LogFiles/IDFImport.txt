
                                   IDF Import
                                   ----------

                       04:29 PM Friday, December 09, 2016
       Job Name: C:\pd_repo\pd\_CITADEL\Mentor\Citadel_Right\PCB\Main.pcb

    Parsing IDF file "C:\pd_repo\pd\_CITADEL\ME\PCBA2-revA_RIGHT_12_8_16.emn".    
    Board thickness will not be imported.    
    Finished parsing IDF file.    
    Placed board outline.    
    Warning: All mounting holes will need their nets reassigned.    
    Placed a new "Non-Plated" mounting hole with diameter "3.260" at (-20.500000,-20.000000).    
    Placed a new "Non-Plated" mounting hole with diameter "3.260" at (0.000000,-36.000000).    
    Placed a new "Non-Plated" mounting hole with diameter "3.260" at (20.500000,-20.000000).    
    Placed 3 drill holes.    
    Successfully imported IDF file with 1 warning.