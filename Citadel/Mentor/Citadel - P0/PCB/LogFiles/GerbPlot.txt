 ----------------------------
|    Gerber Plotting Log     |
|          Started           |
|  Wed Jan 04 12:19:33 2017  |
 ----------------------------


Processing SoldermaskTop.gdo
    Number of Apertures     = 53
    Number of Moves         = 1072
    Number of Draws         = 12
    Number of Arcs          = 0
    Number of Flashes       = 1069
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 3
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 3.2956 (IN)
    Total Move Length       = 377.393 (IN)

Processing SoldermaskBottom.gdo
    Number of Apertures     = 33
    Number of Moves         = 1053
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1053
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 452.255 (IN)

Processing SolderPasteTop.gdo
    Number of Apertures     = 49
    Number of Moves         = 1063
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1063
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 368.172 (IN)

Processing SolderPasteBottom.gdo
    Number of Apertures     = 32
    Number of Moves         = 1041
    Number of Draws         = 0
    Number of Arcs          = 0
    Number of Flashes       = 1041
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 0 (IN)
    Total Move Length       = 441.516 (IN)

Processing GeneratedSilkscreenTop.gdo
    Number of Apertures     = 6
    Number of Moves         = 3076
    Number of Draws         = 63309
    Number of Arcs          = 33
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 1084
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 298.02 (IN)
    Total Move Length       = 1185.42 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing GeneratedSilkscreenBottom.gdo
    Number of Apertures     = 4
    Number of Moves         = 1495
    Number of Draws         = 25747
    Number of Arcs          = 11
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 443
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 110.633 (IN)
    Total Move Length       = 933.621 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer6Bottom.gdo
    Number of Apertures     = 41
    Number of Moves         = 2884
    Number of Draws         = 4742
    Number of Arcs          = 4485
    Number of Flashes       = 1693
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 34
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 206.417 (IN)
    Total Move Length       = 1610.89 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer5.gdo
    Number of Apertures     = 9
    Number of Moves         = 673
    Number of Draws         = 1161
    Number of Arcs          = 3212
    Number of Flashes       = 650
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 6
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 259.379 (IN)
    Total Move Length       = 496.378 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer4.gdo
    Number of Apertures     = 10
    Number of Moves         = 878
    Number of Draws         = 2036
    Number of Arcs          = 3612
    Number of Flashes       = 650
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 46
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 246.442 (IN)
    Total Move Length       = 636.496 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer3.gdo
    Number of Apertures     = 11
    Number of Moves         = 767
    Number of Draws         = 1612
    Number of Arcs          = 3145
    Number of Flashes       = 650
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 21
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 194.666 (IN)
    Total Move Length       = 572.03 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer2.gdo
    Number of Apertures     = 5
    Number of Moves         = 652
    Number of Draws         = 946
    Number of Arcs          = 2648
    Number of Flashes       = 650
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 2
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 280.23 (IN)
    Total Move Length       = 482.296 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing EtchLayer1Top.gdo
    Number of Apertures     = 65
    Number of Moves         = 3100
    Number of Draws         = 5196
    Number of Arcs          = 5465
    Number of Flashes       = 1709
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 55
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 196.002 (IN)
    Total Move Length       = 1531.39 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing DrillDrawingThrough.gdo
    Number of Apertures     = 1
    Number of Moves         = 1812
    Number of Draws         = 45719
    Number of Arcs          = 0
    Number of Flashes       = 0
    Number of Comments      = 5
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 385.405 (IN)
    Total Move Length       = 1229.39 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

Processing BoardOutline.gdo
    Number of Apertures     = 1
    Number of Moves         = 1
    Number of Draws         = 61
    Number of Arcs          = 58
    Number of Flashes       = 0
    Number of Comments      = 4
    Number of Drawn Pads    = 0
    Number of Filled Shapes = 0
    Number of Dcode Warnings= 0
    Number of Dcode Errors  = 0
    Total Draw Length       = 10.2914 (IN)
    Total Move Length       = 1.2271 (IN)

Design has zero width element(s).Replacing with default width (0.005 IN) from gmf file.

 ---------------------------
|    Gerber Plotting Log     |
|          Finished          |
|  Wed Jan 04 12:19:36 2017  |
 ----------------------------
