=======================================================================
PADS Professional Layout - Version 116.0.506.1217
=======================================================================

Job Directory:    C:\pd_repo\pd\_CITADEL\Mentor\Citadel\PCB\


Design Units:(mm)

Test Point Log:   C:\pd_repo\pd\_CITADEL\Mentor\Citadel\PCB\LogFiles\testptlog_04.txt

Fri Dec 02 11:41:42 2016

Net Name        Ref                  X          Y     Side    Location        Operation Result
==============================================================================================
$1N7726         TP34                 -17.9      -25.3 Top     Hanger          Delete              
$1N7726         TP34                 -17.9      -25.3 Top     Hanger          Delete              
BT_PWR_ON       TP39                -19.07     -25.31 Top     Hanger          Delete              
BT_PWR_ON       TP39                -19.07     -25.31 Top     Hanger          Delete              
MFB             TP35                -20.19     -25.29 Top     Hanger          Delete              
MFB             TP35                -20.19     -25.29 Top     Hanger          Delete              
