/* --COPYRIGHT--,BSD
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
//#############################################################################
//
//! \file   CItadel_Demo.c
//
//  Group:          MSP
//  Target Devices: MSP430G2353
//
//  (C) Copyright 2015, Texas Instruments, Inc.

//*****************************************************************************
// Includes
//*****************************************************************************

#include "msp430.h"
#include <stdint.h>
#include "Citadel_Demo.h"

//*****************************************************************************
// Definitions
//*****************************************************************************

//Crusher I/Os and STATE defines
#define CR_BUTTON_IN	(BIT0)   //port 2
#define CR_AMP_EN_OUT	(BIT1)   //port 2
#define CR_GAIN1_OUT	(BIT2)   //port 2
#define CR_GAIN2_OUT    (BIT2)   //port 3
#define CR_STATUS1_OUT  (BIT5)   //port 1
#define CR_STATUS2_OUT  (BIT1)   //port 3
#define CR_STATE_OFF 	(0)
#define CR_STATE_ON  	(5)
#define CR_STATE_GAIN1 	(1)
#define CR_STATE_GAIN2 	(2)
#define CR_STATE_GAIN3 	(3)
#define CR_STATE_GAIN4 	(4)

//ANC I/Os and STATE defines
#define ANC_MONITOR_OUT	(BIT3)	//port 3
#define ANC_OFF_OUT 	(BIT4)	//port 3
#define ANC_BTN_IN		(BIT3)	//port 2
#define ANC_STATE_OUT	(BIT4)	//port 1
#define ANC_STATE_OFF 		(0)
#define ANC_STATE_ON 		(1)
#define ANC_STATE_MONITOR 	(2)

// ADC 2.5V internal reference being used by default.  Therefore, ADC value is 1023*(input voltage  / 2.5V)
#define VBAT_LOW_3_45V  (706)         // 1023 (3.45/2) / 2.5V
#define VBAT_LOW_3_3V   (675)         // 1023 (3.3/2) / 2.5V
#define VBAT_LOW_3_2V   (655)         // 1023 (3.2/2) / 2.5V

// As defined in Citadel_Init(), each Timer tick is .66 msec
#define TIMER_VERY_LONG_CNT (3750)  // ticks to count 2.5 sec
#define TIMER_LONG_CNT 		(1500)	// ticks to count 1 sec
#define TIMER_SHORT_CNT     (150)   // ticks to count 100 msec


//*****************************************************************************
// Global Variables
//*****************************************************************************
static volatile unsigned int uiCrusherPressCnt, uiIsCRButtonPressed, uiCrusherState, uiCrusherNotificationPulseCnt, uiIsCRtoCSRNotificationActive;
static volatile unsigned int uiANCPressCnt, uiIsANCButtonPressed, uiANCState, uiANCNotificationPulseCnt, uiIsANCtoCSRNotificationActive;

//*****************************************************************************
// Function Prototypes
//*****************************************************************************
void Citadel_Init(void);
void Citadel_CrusherProcess(void);
void Citadel_ANCProcess(void);
// This function initializes variables, GPIOs, and Timer
void Citadel_Init(void)
{
	uiCrusherPressCnt = 0;              // keeps count of how long Crusher button is pressed
	uiIsCRButtonPressed = 0;			// 1 = crusher button pressed, 0 = button no pressed
	uiIsCRtoCSRNotificationActive = 0;  // set when new crusher state is entered, means we need to send notification to CSR chip
	uiCrusherNotificationPulseCnt = 0;  // keeps count of crusher notification pulse length to CSR chip
	uiCrusherState = CR_STATE_OFF;      // set initial crusher state to CR_STATE_OFF
	uiANCPressCnt = 0;					// keeps count of how long ANC button is pressed
	uiIsANCButtonPressed=0;				// 1 = ANC button pressed, 0 = button no pressed
	uiANCNotificationPulseCnt = 0;		// keeps count of crusher notification pulse length to CSR chip
	uiIsANCtoCSRNotificationActive=0;	// set when new ANC state is entered, means we need to send notification to CSR chip
	uiANCState = ANC_STATE_OFF;			// set initial ANC state to CR_STATE_OFF


	/*--- Initialize GPIO pin-mux function, direction, and  ---*/
	/*--- inital value for all three ports, P1, P2, and P3  ---*/

	P1SEL2 = (0);  												//used for cap touch, disregard this is handled by CapSense library if needed.
	P1SEL &= ~(CR_STATUS1_OUT + ANC_STATE_OUT);					// Configure P1 GPIO bits
	P1OUT = 0x00;												// Drive all Port 2 pins low
	P1DIR = 0xFF;					     						// Configure all port pins as output

	P2SEL2 = (0);  												//used for cap touch, disregard this is handled by CapSense library if needed.
	P2SEL &= ~(CR_BUTTON_IN + CR_AMP_EN_OUT + CR_GAIN1_OUT + ANC_BTN_IN);	// Configure P2 GPIO bits
	P2OUT = 0x00;												// Drive all Port 2 pins low
	P2DIR &= ~(CR_BUTTON_IN + ANC_BTN_IN);				// Configure all port pins as output except CR_BUTTON_IN
	P2IE |= (CR_BUTTON_IN + ANC_BTN_IN);   				// enable interrupt for CR_BUTTON_IN
	P2IES &= ~(CR_BUTTON_IN + ANC_BTN_IN); 				// initialize low to high transition interrupt
	P2IFG &= ~(CR_BUTTON_IN + ANC_BTN_IN); 				// clear interrupt flag for CR_BUTTON_IN

	P3SEL2 = (0);  												//used for cap touch, disregard this is handled by CapSense library if needed.
	P3SEL &= ~(CR_GAIN2_OUT + CR_STATUS2_OUT + ANC_MONITOR_OUT + ANC_OFF_OUT);	// Configure P3 GPIO bits
	P3OUT = 0x00;												// Drive all Port 2 pins low
	P3DIR = 0xFF;	     										// Configure all port pins as ouputs

	//initalize ADC
	  ADC10AE0 = 0x1;							// enable A0 pin as analog input to save power by eliminating parasitic current
	  ADC10CTL1 = INCH_0 + ADC10DIV_3;         // A0 input channel,  ADC10CLK/4
	  ADC10CTL0 = REFBURST + REF2_5V + ADC10SHT_3 + REFON + ADC10ON + ADC10IE; // REFBURST saves power, REF2_5V and REFON bits needed for internal 2.5V
	  __delay_cycles(1000);                     // Wait for ADC Ref to settle
	  ADC10CTL0 |= ENC + ADC10SC;               // Sampling and conversion start

	// initialize timer 1, timer 0 and watchdog timer are used by proximity sensor
	TA1CTL = MC_0 | TACLR; 						// Stop timer and clear
    TA1CTL |= TASSEL_1 + MC_2 + ID_3 + TAIE;  	// Start timer, source = ACLK, Continous mode, /8, timer_overflow interrupt enabled
    // since ACLK = VLO (12 KHz) and we divided by 8, each clock tick is 12 KHz/ 8 = 1.5 Khz or .67 msec
}

void Citadel_CrusherProcess(void)
{

	if (P2IN & CR_BUTTON_IN) //button pressed
	{
		uiCrusherPressCnt = 0; //start counting how long button is pressed
		uiIsCRButtonPressed = 1;
	}
	else
	{  // button released
		uiIsCRButtonPressed = 0;
		if (uiCrusherPressCnt > TIMER_VERY_LONG_CNT)
		{
			// Do nothing, this scenerio is taken care of in the timer ISR
		}
		else
		{
			// a crusher state is guarateed to change hence we can reset the following variables here
			uiIsCRtoCSRNotificationActive = 1;  // indicate we need to sent a notification to CSR chip
			uiCrusherNotificationPulseCnt = 0;  // keep count of how long pulse needs to be.

			if (uiCrusherState > CR_STATE_OFF)  // if crusher is enabled
			{
				//increase crusher gain
				switch (uiCrusherState)
				{
				case CR_STATE_ON:
					//start at 00 or CR_STATE_GAIN1
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT &= (~CR_GAIN2_OUT);
					uiCrusherState = CR_STATE_GAIN1;  	// update state
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN1:
					// we are in step1 (00), we want to go to step 2 (10)
					P2OUT |= CR_GAIN1_OUT;
					uiCrusherState = CR_STATE_GAIN2;	// update state
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN2:
					// we are in step 2 (10) we want to go to step 3 (01)
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT |= CR_GAIN2_OUT;
					uiCrusherState = CR_STATE_GAIN3;	// update status
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN3:
					// we are in step3 (01) and want to go to step 4 (11)
					P2OUT |= CR_GAIN1_OUT;
					uiCrusherState = CR_STATE_GAIN4;	// update status
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN4:
					// we are in step4 (11) and want to go to step 0 (00)
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT &= (~CR_GAIN2_OUT);
					uiCrusherState = CR_STATE_GAIN1; 	// update state
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					break;
				default:
					break;
				}
			}
			else
			{
				P2OUT |= CR_AMP_EN_OUT;         // turn on crusher
				uiCrusherState = CR_STATE_ON;	// update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
}

void Citadel_ANCProcess(void)
{

	if (P2IN & ANC_BTN_IN) //button pressed
	{
		uiANCPressCnt = 0; //start counting how long button is pressed
		uiIsANCButtonPressed = 1;
	}
	else  // button released
	{
		uiIsANCButtonPressed = 0;
		if (uiANCPressCnt > TIMER_VERY_LONG_CNT)
		{
			// Do nothing, this scenerio is taken care of in the timer ISR
		}
		else
		{
			// a ANC state is guarateed to change hence we can reset the following variables here
			uiIsANCtoCSRNotificationActive = 1;  	// indicate we need to sent a notification to CSR chip
			uiANCNotificationPulseCnt = 0;  		// keep count of how long pulse needs to be.

			if (uiANCState > ANC_STATE_OFF)  // if crusher is enabled
			{
				//increase crusher gain
				switch (uiANCState)
				{
				case ANC_STATE_ON:
					P3OUT |= ANC_MONITOR_OUT;
					uiCrusherState = ANC_STATE_MONITOR;  	// update state
					P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
					break;
				case ANC_STATE_MONITOR:
					P3OUT &= ~ANC_MONITOR_OUT;
					uiCrusherState = ANC_STATE_ON;	// update state
					P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
					break;
				default:
					break;
				}
			}
			else
			{
				P2OUT &= ~(ANC_OFF_OUT);         // turn on ANC
				uiCrusherState = ANC_STATE_ON;	// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
}

#pragma vector=PORT2_VECTOR
__interrupt void ISR_Port2(void)
{

	if (P2IFG & CR_BUTTON_IN)  		//check if it's a crusher button interrupt
	{
		P2IFG &= ~CR_BUTTON_IN; 	// P1.3 IFG cleared
		P2IES ^= CR_BUTTON_IN; 		// toggle the interrupt edge,
		Citadel_CrusherProcess();
	}
	else if (P2IFG & ANC_BTN_IN)  		// check if it's a ANC button interrupt
	{
		P2IFG &= ~ANC_BTN_IN; 		// P1.3 IFG cleared
		P2IES ^= ANC_BTN_IN; 		// toggle the interrupt edge,
		Citadel_ANCProcess();
	}
}

#pragma vector=TIMER1_A1_VECTOR
__interrupt void ISR_Timer1_A1(void)
{
	if(TA1IV & TA1IV_TAIFG)
	{
		uiCrusherPressCnt++;
		uiANCPressCnt++;
		uiCrusherNotificationPulseCnt++;
	}

	if (uiIsCRButtonPressed)
	{
		if (uiCrusherPressCnt > TIMER_VERY_LONG_CNT)
		{
			// a crusher state is guarateed to change hence we can reset the following variables here
			uiIsCRtoCSRNotificationActive = 1;  // indicate we need to sent a notification to CSR chip
			uiCrusherNotificationPulseCnt = 0;  // keep count of how long pulse needs to be.

			if (P2OUT & CR_AMP_EN_OUT)  // if crusher is enabled
			{
				P2OUT &= (~CR_AMP_EN_OUT);      // turn off crusher
				uiCrusherState = CR_STATE_OFF;  // update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
			else
			{
				P2OUT |= CR_AMP_EN_OUT;         // turn on crusher
				uiCrusherState = CR_STATE_ON;	// update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
	else if (uiIsANCButtonPressed)
	{
		if (uiANCPressCnt > TIMER_VERY_LONG_CNT)
		{
			if ((P2OUT & ANC_OFF_OUT) == 0)  	// if ANC is enabled
			{
				P2OUT |= ANC_OFF_OUT;        	// turn off ANC
				uiANCState = ANC_STATE_OFF;  	// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip

			}
			else
			{
				P2OUT &= ~ANC_OFF_OUT;    		// turn on ANC
				P3OUT &= ~ANC_MONITOR_OUT;		// turn off monitor
				uiANCState = ANC_STATE_ON;		// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}

	// TODO: should we make this an elseif to reduce time in ISR?
	if (uiIsCRtoCSRNotificationActive)  // takes priority over ANC hence put ANC notifications in else statement.
	{
		switch (uiCrusherState)
		{
			case CR_STATE_ON:
				if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
				}
				break;
			case CR_STATE_OFF:
				if (uiCrusherNotificationPulseCnt > TIMER_VERY_LONG_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
				}
				break;
			case CR_STATE_GAIN1:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
				}
				break;
			case CR_STATE_GAIN2:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
				}
				break;
			case CR_STATE_GAIN3:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
				}
				break;
			case CR_STATE_GAIN4:
				if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
				}
				break;
			default:
				break;
		}
	}
	else if (uiIsANCtoCSRNotificationActive)  // check for pending ANC notifications
	{
		switch (uiANCState)
		{
		case ANC_STATE_ON:
			if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
			}
			break;
		case ANC_STATE_OFF:
			if (uiCrusherNotificationPulseCnt > TIMER_VERY_LONG_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
			}
			break;
		case ANC_STATE_MONITOR:
			if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
			}
			break;
		default:
			break;
		}
	}
}




