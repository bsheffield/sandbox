/* --COPYRIGHT--,BSD
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
//#############################################################################
//
//! \file   CItadel_Demo.c
//
//  Group:          MSP
//  Target Devices: MSP430G2353
//
//  (C) Copyright 2015, Texas Instruments, Inc.

//*****************************************************************************
// Includes
//*****************************************************************************

#include "msp430.h"
#include <stdint.h>
#include "Citadel_Demo.h"

//#define LAUNCHPAD_TEST // comment out when not using launchpad.

//*****************************************************************************
// Definitions
//*****************************************************************************

//Crusher I/Os and STATE defines
#define CR_BUTTON_IN	(BIT0)   //port 2
#define CR_AMP_EN_OUT	(BIT1)   //port 2
#define CR_GAIN1_OUT	(BIT2)   //port 2
#define CR_GAIN2_OUT    (BIT2)   //port 3
#define CR_STATUS1_OUT  (BIT5)   //port 1
#define CR_STATUS2_OUT  (BIT1)   //port 3
#define CR_STATE_OFF 	(0)
#define CR_STATE_ON  	(5)
#define CR_STATE_GAIN1 	(1)
#define CR_STATE_GAIN2 	(2)
#define CR_STATE_GAIN3 	(3)
#define CR_STATE_GAIN4 	(4)

//ANC I/Os and STATE defines
#define ANC_MONITOR_OUT	(BIT3)	//port 3
#define ANC_OFF_OUT 	(BIT4)	//port 3
#define ANC_BTN_IN		(BIT3)	//port 2
#define ANC_STATE_OUT	(BIT4)	//port 1
#define ANC_STATE_OFF 		(0)
#define ANC_STATE_ON 		(1)
#define ANC_STATE_MONITOR 	(2)

// Other I/Os and STATE
#define LOW_BAT  (BIT2)  // port 1
#define VBAT_STATE_INIT		(0)
#define VBAT_STATE_3_45V	(1)
#define VBAT_STATE_3_3V		(2)
#define VBAT_STATE_3_2V		(3)
#define VBAT_STATE_WAIT		(4)
#define VBAT_STATE_3_2VLONG (5)
#define VREG				(BIT1)  //PORT 1
#define CHG_STAT			(BIT0)  //PORT 3
#define BT_PWR_ON			(BIT3)  //PORT 1
#define TP_SW				(BIT7)  //PORT 1
#define CSR_LED_DIS			(BIT6)  //PORT 1
#define LED_RED				(BIT7)  //PORT 3
#define LED1				(BIT6)  //PORT 3
#define LED2				(BIT5)  //PORT 3
#define LED3				(BIT5)  //PORT 2
#define LED_STATE_ALL_OFF	(0)
#define LED_STATE_0to40		(1)
#define LED_STATE_40to70	(2)
#define LED_STATE_70to99	(3)
#define LED_STATE_100		(4)
#define LED_STATE_10RED		(5)
#define LED_STATE_WAIT30SEC	(6)

// ADC 2.5V internal reference being used by default.  Therefore, ADC value is 1023*(input voltage  / 2.5V)
// ADC input is 1/2 of VBAT value per schematics
#define VBAT_100_Percent (859)     	// VBAT = 4.2V
#define VBAT_70_Percent  (798)
#define VBAT_40_Percent  (737)
#define VBAT_10_Percent	 (675)
#define VBAT_0_Percent   (655)		// VBAT = 3.2
#define VBAT_FULL_4_1V   (839)		// anything above 4.1V is considered 100% charged
#define VBAT_LOW_3_45V   (706)      // 1023 (3.45/2) / 2.5V
#define VBAT_LOW_3_3V    (675)      // 1023 (3.3/2) / 2.5V
#define VBAT_LOW_3_2V    (655)      // 1023 (3.2/2) / 2.5V


// As defined in Citadel_Init(), each Timer tick is .66 msec, we are triggering ISR every 50 msec
#define TIMER_1_ISR_DELAY 	(5000)	// ~ 50 msec was 75 on ACLK, but now using SMCLK
#define TIMER_VERY_LONG_CNT (50)  // ticks to count 2.5 sec
#define TIMER_LONG_CNT 		(20)	// ticks to count 1 sec
#define TIMER_SHORT_CNT     (2)   // ticks to count 100 msec
#define TIMER_MIN_CNT		(1200) // ticks in one minute
#define TIMER_30SEC_CNT		(600) // ticks in 30 sec
#define CHECK_VBAT_LEVEL_INTERVAL   (5)  // unit is minutes
//*****************************************************************************
// Global Variables
//*****************************************************************************
static volatile unsigned int uiCrusherPressCnt, uiIsCRButtonPressed, uiCrusherState, uiCrusherNotificationPulseCnt, uiIsCRtoCSRNotificationActive;
static volatile unsigned int uiANCPressCnt, uiIsANCButtonPressed, uiANCState, uiANCNotificationPulseCnt, uiIsANCtoCSRNotificationActive;
static long lADCCnt;
static volatile unsigned int uiADCMinuteCnt, uiVBATValue, uiVBAT_State, uiVBAT_PulseCnt, uiVBAT_3_45V_Pulse, uiVBAT_3_3V_Pulse, uiVBAT_3_2V_Pulse, uiIsVBATNotificationActive;
static volatile unsigned int uiBTisON, ui_3_5mm_JackInserted, uiChargeStatusCharging, uiChargeStatusCnt, uiLEDState, uiVREGButtonPressed, uiRedLEDFlashCnt, uiRedLEDWaitCnt;

#ifdef LAUNCHPAD_TEST
volatile unsigned int uiT1A0ISRcalled, uiADC10ISRcalled, uiP1ISRcalled, uiP2ISRcalled, uiP2ISRcalled1, uiP2ISRcalled2;
#else
volatile unsigned int uiT1A0ISRcalled, uiADC10ISRcalled, uiP1_TP_SW_ISRcalled, uiP1_BT_PWR_ON_ISRcalled, uiP1_VREG_ISRcalled, uiP2_CR_BTN_ISRcalled, uiP2_ANC_BTN_ISRcalled;
#endif

//todo delete all references to Citadel_ISRTim1Cnt
static unsigned int Citadel_ISRTim1Cnt;


//*****************************************************************************
// Function Prototypes
//*****************************************************************************
void Citadel_Init(void);
int Citadel_Process(void);
void T1A0_Task(void);
void ADC10_Task(void);
void P1_Task(void);
void P2_Task(void);
void Citadel_CrusherProcess(void);
void Citadel_ANCProcess(void);

// This function initializes variables, GPIOs, and Timer
void Citadel_Init(void)
{
	Citadel_ISRTim1Cnt = 0;

	uiT1A0ISRcalled = 0;
	uiADC10ISRcalled = 0;

	uiCrusherPressCnt = 0;              // keeps count of how long Crusher button is pressed
	uiIsCRButtonPressed = 0;			// 1 = crusher button pressed, 0 = button no pressed
	uiIsCRtoCSRNotificationActive = 0;  // set when new crusher state is entered, means we need to send notification to CSR chip
	uiCrusherNotificationPulseCnt = 0;  // keeps count of crusher notification pulse length to CSR chip
	uiCrusherState = CR_STATE_OFF;      // set initial crusher state to CR_STATE_OFF

	uiANCPressCnt = 0;					// keeps count of how long ANC button is pressed
	uiIsANCButtonPressed=0;				// 1 = ANC button pressed, 0 = button no pressed
	uiANCNotificationPulseCnt = 0;		// keeps count of crusher notification pulse length to CSR chip
	uiIsANCtoCSRNotificationActive=0;	// set when new ANC state is entered, means we need to send notification to CSR chip
	uiANCState = ANC_STATE_OFF;			// set initial ANC state to CR_STATE_OFF

	lADCCnt = 0;						// keeps ticker clock count (long variable needed to count a minute of ticks)
	uiADCMinuteCnt = 0;					// Minutes count
	uiVBATValue = 0;
	uiVBAT_State = VBAT_STATE_INIT;
	uiVBAT_3_45V_Pulse = 0;
	uiVBAT_3_3V_Pulse = 0;
	uiVBAT_3_2V_Pulse = 0;
	uiVBAT_PulseCnt =0;
	uiIsVBATNotificationActive = 0;

	uiVREGButtonPressed = 0;
	uiChargeStatusCnt = 0;
	uiChargeStatusCharging = 0;
	uiLEDState = LED_STATE_ALL_OFF;
	uiRedLEDFlashCnt = 0;
	uiRedLEDWaitCnt = 0;
	ui_3_5mm_JackInserted = 0;
	uiBTisON = 0;

	  TA1CCTL0 = CCIE;                             // CCR0 interrupt enabled
	  TA1CCR0 = TIMER_1_ISR_DELAY-1;
	  //TA1CTL = TASSEL_1 + MC_1 + ID_3 + TACLR;                  // ACLK, upmode  BS commented out
	  TA1CTL = TASSEL_2 + MC_1;// + TACLR;  //BS test ACLK.          //SMCLK (1MHz)

//	CCTL0 = CCIE;                             // CCR0 interrupt enabled
//	CCR0 = TIMER_1_ISR_DELAY-1;
//	TACTL = TASSEL_1 + MC_1 + ID_3 + TACLR;                  // ACLK, upmode


#ifdef	LAUNCHPAD_TEST
	P1DIR |= BIT0 + BIT6;                     // Set LED GPIOs as output
	P1DIR &= ~BIT3;  							// input

	P1IE |=  BIT3;                            // P1.3 interrupt enabled
	P1IES |= BIT3;                            // P1.3 Hi/lo edge
	P1OUT |= BIT3;
	P1REN |= BIT3;							// Enable Pull Up on SW2 (P1.3)
	P1IFG &= ~BIT3;                           // P1.3 IFG cleared

	P2DIR &= ~BIT0;					// BIT 3 used for ADC

	P2IE |=  BIT0 + BIT1 + BIT2;                            // P1.3 interrupt enabled
	P2IES |= BIT0 + BIT1 + BIT2;                            // P1.3 Hi/lo edge
	P2OUT &= ~(BIT0 + BIT1 + BIT2);
	P2REN |= BIT0 + BIT1 + BIT2;							// Enable Pull Up on SW2 (P1.3)
	P2IFG &= ~(BIT0 + BIT1 + BIT2);                           // P1.3 IFG cleared

	ADC10AE0 = BIT5;						// enable A0 pin as analog input to save power by eliminating parasitic current
	ADC10CTL1 = INCH_5 + ADC10DIV_3;        // A0 input channel,  ADC10CLK/4
	ADC10CTL0 =  ADC10SHT_3 + REF2_5V + REFON + ADC10ON + ADC10IE;  // REFBURST saves power, REF2_5V and REFON bits needed for internal 2.5V
	__delay_cycles(1000);    					// Wait for ADC Ref to settle

#else

	/*--- Initialize GPIO pin-mux function, direction, and  ---*/
	/*--- inital value for all three ports, P1, P2, and P3  ---*/

    // set all P1 pins as GPIO...P1.0 is ADC, but this gets taken care of later in the code
	P1SEL2 = 0x00;
	P1SEL = 0x00;// | BIT4;
	P1DIR = 0x00;// | BIT4; 									// set all pins to outputs initially
	P1DIR &= ~(TP_SW + BT_PWR_ON + VREG + BIT0);  	// change the following pins to inputs, BIT0 is ADC which is input
	P1IE = (TP_SW + BT_PWR_ON + VREG);				// enable interrupts for following pins
	P1IES &= ~(TP_SW + BT_PWR_ON + VREG);			// Lo/Hi edge
	P1OUT = 0x00;									// Drive all Port 2 pins low
//	P1REN = 0xFF;									// enable internal pull-downs so that no pin is left floating
	P1IFG &= ~(TP_SW + BT_PWR_ON + VREG);			// clear interrupt flags

	// set all P2 pins as GPIO.  p2.4 used for cap touch, this is handled by CapSense library if needed.
	P2SEL2 = 0x00;
//	P2SEL = BIT4;									// 2.4 is cap sense, all others are GPIO
    P2DIR = 0x00;									// set all pins to outputs initially
	P2DIR &= ~(CR_BUTTON_IN + ANC_BTN_IN);			// change the following pins to inputs
	P2IE = (CR_BUTTON_IN + ANC_BTN_IN);   			// enable interrupt for CR_BUTTON_IN
	P2IES &= ~(CR_BUTTON_IN + ANC_BTN_IN); 			// initialize low to high transition interrupt
    P2OUT = 0x00;
    P2REN = 0x7F;
	P2IFG &= ~(CR_BUTTON_IN + ANC_BTN_IN); 			// clear interrupt flag for CR_BUTTON_IN

	// set all P3 pins as GPIO.
	P3SEL2 = 0x00;
	P3SEL = 0x00;
    P3DIR = 0x00;									// set all pins to outputs initially
	P3DIR &= ~(CHG_STAT);							// change the following pins to inputs 							// initialize low to high transition interrupt
    P3OUT = 0x00;
    P3REN = 0xFF;
			 				// clear interrupt flag for CR_BUTTON_IN

	//initalize ADC
	ADC10AE0 = 0x1;							// enable A0 pin as analog input to save power by eliminating parasitic current
	ADC10CTL1 = INCH_0 + ADC10DIV_3;         // A0 input channel,  ADC10CLK/4
	ADC10CTL0 = ADC10SHT_3 + REF2_5V + REFON + ADC10ON + ADC10IE;  // REFBURST saves power, REF2_5V and REFON bits needed for internal 2.5V
	__delay_cycles(1000);    // Wait for ADC Ref to settle

#endif

}

int Citadel_Process(void)
{

	__bis_SR_register(LPM1_bits + GIE);

#ifdef	LAUNCHPAD_TEST
	if (uiT1A0ISRcalled)
	{
		uiT1A0ISRcalled = 0;
	}
	else if (uiP1ISRcalled)
	{
		uiP1ISRcalled = 0;
	}
	else if (uiP2ISRcalled)
	{
		uiP2ISRcalled = 0;
		ADC10CTL0 |= ENC + ADC10SC;
	}
	else if (uiP2ISRcalled1)
	{
		uiP2ISRcalled1 = 0;
	}
	else if (uiP2ISRcalled2)
	{
		uiP2ISRcalled2 = 0;
	}
#else
	if (uiT1A0ISRcalled)
	{
		uiT1A0ISRcalled = 0;
		T1A0_Task();
	}
	else if (uiP1_TP_SW_ISRcalled)
	{
		uiP1_TP_SW_ISRcalled = 0;  // TODO: headphone was either plugged/unplugged..what action do we take
	}
	else if (uiP1_BT_PWR_ON_ISRcalled)
	{
		uiP1_BT_PWR_ON_ISRcalled = 0;  // TODO: BT is either on or off, what action do we take
	}
	else if (uiP1_VREG_ISRcalled)
	{
		uiP1_VREG_ISRcalled = 0;
		ADC10CTL0 |= ENC + ADC10SC;               // Sampling and conversion start
		uiVREGButtonPressed = 1;
	}
	else if (uiP2_CR_BTN_ISRcalled)
	{
		uiP2_CR_BTN_ISRcalled = 0;
		Citadel_CrusherProcess();
	}
	else if (uiP2_ANC_BTN_ISRcalled)
	{
		uiP2_ANC_BTN_ISRcalled = 0;
		Citadel_ANCProcess();
	}
#endif
	else if (uiADC10ISRcalled)
	{
		uiADC10ISRcalled = 0;
//		ADC10_Task();
	}

    return Citadel_ISRTim1Cnt;
}

void Citadel_CrusherProcess(void)
{

	if (P2IN & CR_BUTTON_IN) //button pressed
	{
		uiCrusherPressCnt = 0; //start counting how long button is pressed
		uiIsCRButtonPressed = 1;
	}
	else
	{  // button released
		uiIsCRButtonPressed = 0;

		if (uiCrusherPressCnt > TIMER_VERY_LONG_CNT)
		{
			// Do nothing, this scenerio is taken care of in the timer ISR
		}
		else
		{
			// a crusher state is guarateed to change hence we can reset the following variables here
			uiIsCRtoCSRNotificationActive = 1;  // indicate we need to sent a notification to CSR chip
			uiCrusherNotificationPulseCnt = 0;  // keep count of how long pulse needs to be.

			if (uiCrusherState > CR_STATE_OFF)  // if crusher is enabled
			{
				//increase crusher gain
				switch (uiCrusherState)
				{
				case CR_STATE_ON:
					//start at 00 or CR_STATE_GAIN1
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT &= (~CR_GAIN2_OUT);
					uiCrusherState = CR_STATE_GAIN1;  	// update state
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN1:
					// we are in step1 (00), we want to go to step 2 (10)
					P2OUT |= CR_GAIN1_OUT;
					uiCrusherState = CR_STATE_GAIN2;	// update state
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN2:
					// we are in step 2 (10) we want to go to step 3 (01)
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT |= CR_GAIN2_OUT;
					uiCrusherState = CR_STATE_GAIN3;	// update status
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN3:
					// we are in step3 (01) and want to go to step 4 (11)
					P2OUT |= CR_GAIN1_OUT;
					uiCrusherState = CR_STATE_GAIN4;	// update status
					P3OUT |= CR_STATUS2_OUT;			// start state change pulse notification to CSR chip
					break;
				case CR_STATE_GAIN4:
					// we are in step4 (11) and want to go to step 0 (00)
					P2OUT &= (~CR_GAIN1_OUT);
					P3OUT &= (~CR_GAIN2_OUT);
					uiCrusherState = CR_STATE_GAIN1; 	// update state
					P1OUT |= CR_STATUS1_OUT; 			// start state change pulse notification to CSR chip
					break;
				default:
					break;
				}
			}
			else
			{
				P2OUT |= CR_AMP_EN_OUT;         // turn on crusher
				uiCrusherState = CR_STATE_ON;	// update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
}

void Citadel_ANCProcess(void)
{

	if (P2IN & ANC_BTN_IN) //button pressed
	{
		uiANCPressCnt = 0; //start counting how long button is pressed
		uiIsANCButtonPressed = 1;
	}
	else  // button released
	{
		uiIsANCButtonPressed = 0;
		if (uiANCPressCnt > TIMER_VERY_LONG_CNT)
		{
			// Do nothing, this scenerio is taken care of in the timer ISR
		}
		else
		{
			// a ANC state is guarateed to change hence we can reset the following variables here
			uiIsANCtoCSRNotificationActive = 1;  	// indicate we need to sent a notification to CSR chip
			uiANCNotificationPulseCnt = 0;  		// keep count of how long pulse needs to be.

			if (uiANCState > ANC_STATE_OFF)  // if crusher is enabled
			{
				//increase crusher gain
				switch (uiANCState)
				{
				case ANC_STATE_ON:
					P3OUT |= ANC_MONITOR_OUT;
					uiCrusherState = ANC_STATE_MONITOR;  	// update state
					P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
					break;
				case ANC_STATE_MONITOR:
					P3OUT &= ~ANC_MONITOR_OUT;
					uiCrusherState = ANC_STATE_ON;	// update state
					P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
					break;
				default:
					break;
				}
			}
			else
			{
				P2OUT &= ~(ANC_OFF_OUT);         // turn on ANC
				uiCrusherState = ANC_STATE_ON;	// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
}

void T1A0_Task(void)
{
	uiCrusherPressCnt++;
	uiANCPressCnt++;
	uiCrusherNotificationPulseCnt++;
	lADCCnt++;
	uiVBAT_PulseCnt++;
	uiChargeStatusCnt++;
	uiRedLEDWaitCnt++;
	if (lADCCnt == TIMER_MIN_CNT)
	{
		lADCCnt = 0;
		uiADCMinuteCnt++;
	}

	if (uiIsCRButtonPressed)
	{
		if (uiCrusherPressCnt > TIMER_VERY_LONG_CNT)
		{
			// a crusher state is guarateed to change hence we can reset the following variables here
			uiIsCRtoCSRNotificationActive = 1;  // indicate we need to sent a notification to CSR chip
			uiCrusherNotificationPulseCnt = 0;  // keep count of how long pulse needs to be.

			if (P2OUT & CR_AMP_EN_OUT)  // if crusher is enabled
			{
				P2OUT &= (~CR_AMP_EN_OUT);      // turn off crusher
				uiCrusherState = CR_STATE_OFF;  // update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
			else
			{
				P2OUT |= CR_AMP_EN_OUT;         // turn on crusher
				uiCrusherState = CR_STATE_ON;	// update state
				P1OUT |= CR_STATUS1_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}
	else if (uiIsANCButtonPressed)
	{
		if (uiANCPressCnt > TIMER_VERY_LONG_CNT)
		{
			if ((P2OUT & ANC_OFF_OUT) == 0)  	// if ANC is enabled
			{
				P2OUT |= ANC_OFF_OUT;        	// turn off ANC
				uiANCState = ANC_STATE_OFF;  	// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip

			}
			else
			{
				P2OUT &= ~ANC_OFF_OUT;    		// turn on ANC
				P3OUT &= ~ANC_MONITOR_OUT;		// turn off monitor
				uiANCState = ANC_STATE_ON;		// update state
				P1OUT |= ANC_STATE_OUT; 		// start state change pulse notification to CSR chip
			}
		}
	}

	// TODO: should we make this an elseif to reduce time in ISR?
	if (uiIsCRtoCSRNotificationActive)  // takes priority over ANC hence put ANC notifications in else statement.
	{
		switch (uiCrusherState)
		{
			case CR_STATE_ON:
				if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			case CR_STATE_OFF:
				if (uiCrusherNotificationPulseCnt > TIMER_VERY_LONG_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			case CR_STATE_GAIN1:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			case CR_STATE_GAIN2:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			case CR_STATE_GAIN3:
				if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
					P1OUT &= ~CR_STATUS1_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			case CR_STATE_GAIN4:
				if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
				{
					P3OUT &= ~CR_STATUS2_OUT; 		// end pulse notification to CSR chip
					uiIsCRtoCSRNotificationActive = 0;
				}
				break;
			default:
				break;
		}
	}
	else if (uiIsANCtoCSRNotificationActive)  // check for pending ANC notifications
	{
		switch (uiANCState)
		{
		case ANC_STATE_ON:
			if (uiCrusherNotificationPulseCnt > TIMER_SHORT_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
				uiIsANCtoCSRNotificationActive = 0;
			}
			break;
		case ANC_STATE_OFF:
			if (uiCrusherNotificationPulseCnt > TIMER_VERY_LONG_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
				uiIsANCtoCSRNotificationActive = 0;
			}
			break;
		case ANC_STATE_MONITOR:
			if (uiCrusherNotificationPulseCnt > TIMER_LONG_CNT)
			{
				P1OUT &= ~ANC_STATE_OUT; 		// end pulse notification to CSR chip
				uiIsANCtoCSRNotificationActive = 0;
			}
			break;
		default:
			break;
		}
	}
	else if (uiIsVBATNotificationActive)  // check for pending VBAT notifications
	{
		    switch (uiVBAT_State)
			{
			case VBAT_STATE_3_45V:
				if (uiVBAT_PulseCnt > TIMER_SHORT_CNT)
				{
					P1OUT &= ~LOW_BAT; 		// end pulse notification to CSR chip
					uiIsVBATNotificationActive = 0;
				}
				break;
			case VBAT_STATE_3_3V:
				if (uiVBAT_PulseCnt > TIMER_SHORT_CNT)
				{
					P1OUT &= ~LOW_BAT; 		// end pulse notification to CSR chip
					uiIsVBATNotificationActive = 0;
				}
				break;
			case VBAT_STATE_3_2V:
				if (uiVBAT_PulseCnt > TIMER_SHORT_CNT)
				{
					P1OUT &= ~LOW_BAT; 		// end pulse notification to CSR chip
					uiVBAT_PulseCnt = 0;
					uiVBAT_State = VBAT_STATE_WAIT;
				}
				break;
			case VBAT_STATE_WAIT:
				if (uiVBAT_PulseCnt > TIMER_VERY_LONG_CNT)
				{
					P1OUT |= LOW_BAT; 		// start long pulse notification to CSR chip
					uiVBAT_PulseCnt = 0;
					uiVBAT_State = VBAT_STATE_3_2VLONG;
				}
				break;
			case VBAT_STATE_3_2VLONG:
				if (uiVBAT_PulseCnt > TIMER_VERY_LONG_CNT)
				{
					P1OUT &= ~LOW_BAT; 		// end pulse notification to CSR chip
					uiIsVBATNotificationActive = 0;
				}
				break;
			default:
				break;
			}
	}
	else if (uiChargeStatusCharging || uiVREGButtonPressed)
	{
		switch (uiLEDState)
		{
			case LED_STATE_70to99:
				if (uiChargeStatusCnt > (TIMER_LONG_CNT/2))
				{
					P2OUT &= ~LED3;
				}
				break;
			case LED_STATE_40to70:
				if (uiChargeStatusCnt > (TIMER_LONG_CNT/2))
				{
					P3OUT &= ~LED2;
				}
				break;
			case LED_STATE_0to40:
				if (uiChargeStatusCnt > (TIMER_LONG_CNT/2))
				{
					P3OUT &= ~(LED1 + LED_RED);
				}
				break;
			case LED_STATE_10RED:
				if (uiChargeStatusCnt > (TIMER_SHORT_CNT/2))
				{
					P3OUT &= ~(LED1 + LED_RED);
					uiRedLEDFlashCnt++;
					if (uiRedLEDFlashCnt >= 3)
					{
						uiLEDState = LED_STATE_WAIT30SEC;
						uiRedLEDWaitCnt = 0;
					}
				}
			case LED_STATE_WAIT30SEC:
				if (uiRedLEDWaitCnt > TIMER_30SEC_CNT)  // 30 seconds
				{
					uiLEDState = LED_STATE_10RED;
					uiRedLEDFlashCnt = 0;
				}
				break;
			default:
				break;
		}
		uiVREGButtonPressed = 0;
	}

#if 0
	if (uiChargeStatusCnt > TIMER_LONG_CNT)   // TODO: do we want to check more often? check state of charge status pin
	{

		uiChargeStatusCharging = P3IN & CHG_STAT;
		if (uiChargeStatusCharging)
		{
			ADC10CTL0 |= ENC + ADC10SC;               // Sampling and conversion start
			uiADCMinuteCnt = 0;
			uiChargeStatusCnt = 0;
			uiRedLEDFlashCnt = 0;
		}
		else if (uiLEDState < LED_STATE_10RED)
		{
		   // clear all LEDs
		   P3OUT &= ~(LED1 + LED2 + LED_RED);
		   P2OUT &= ~LED3;
		   uiLEDState = LED_STATE_ALL_OFF;
		   uiChargeStatusCnt = 0;
		   uiRedLEDFlashCnt = 0;
		}
		else if (uiLEDState == LED_STATE_10RED) // means we are not charging and we are in the sequence of 3 red led flashes and wait 30 sec
		{
			ADC10CTL0 |= ENC + ADC10SC;               // Sampling and conversion start
			uiChargeStatusCnt = TIMER_LONG_CNT - TIMER_SHORT_CNT;  // we want to flash RED LED faster, hence not resetting count all the way to zero
		}
	}

	if (uiADCMinuteCnt >= CHECK_VBAT_LEVEL_INTERVAL)
	{
		ADC10CTL0 |= ENC + ADC10SC;               // Sampling and conversion start
		uiADCMinuteCnt = 0;
	}
#endif
}

void  ADC10_Task(void)
{
if (uiChargeStatusCharging || uiVREGButtonPressed)
   {
	   if (uiVBATValue > VBAT_FULL_4_1V )
	   {
		   //turn on all LEDs
		   P3OUT |= LED1 + LED2;
		   P2OUT |= LED3;
		   uiLEDState = LED_STATE_100;
	   }
	   else if (uiVBATValue > VBAT_70_Percent)
	   {
		   //turn on all LEDs
		   P3OUT |= LED1 + LED2;  //solid
		   P2OUT |= LED3;    // flashing
		   uiLEDState = LED_STATE_70to99;
	   }
	   else if (uiVBATValue > VBAT_40_Percent)
	   {
		   //turn on all LEDs
		   P3OUT |= LED1 + LED2;  //led1 solid, led2 flashing.
		   P2OUT &= ~LED3;    // clear LED (should be cleared already but just in case)
		   uiLEDState = LED_STATE_40to70;
	   }
	   else
	   {
		   if ((uiVBATValue < VBAT_10_Percent) && uiVREGButtonPressed)
		   {
			   P3OUT |= LED_RED;
		   }
		   else
		   {
			   P3OUT |= LED1;  //flashing
			   P3OUT &= ~(LED2 + LED_RED);     // clear LED (should be cleared already but just in case)
			   P2OUT &= ~LED3;     // clear LED (should be cleared already but just in case)
		   }
		   uiLEDState = LED_STATE_0to40;
	   }
   }
   else if ((uiVBATValue <=  VBAT_LOW_3_2V) && (uiVBAT_3_2V_Pulse == 0))  //check if VBAT level and send pulse if it has not been sent already
   {
   	   //start short pulse
   	   P1OUT |= LOW_BAT;
   	   uiVBAT_State = VBAT_STATE_3_2V;
   	   uiVBAT_3_2V_Pulse = 1;
   	   uiIsVBATNotificationActive = 1;
   	   uiVBAT_PulseCnt = 0;
   }
   else if (uiVBATValue < VBAT_10_Percent)
   {
	   // 3 quick flashes then wait 30 seconds
	   P3OUT |= LED_RED;
	   uiLEDState = LED_STATE_10RED;
   }
   else if ((uiVBATValue <  VBAT_LOW_3_3V) && (uiVBAT_3_3V_Pulse == 0))
   {
 	   // start short pulse
	   P1OUT |= LOW_BAT;
	   uiVBAT_State = VBAT_STATE_3_3V;
	   uiVBAT_3_3V_Pulse = 1;
	   uiVBAT_3_2V_Pulse = 0;  //if it made it into this else, it means VBAT > 3.2V thus we need to clear pulse flag so we can send pulse again
   	   uiIsVBATNotificationActive = 1;
   	   uiVBAT_PulseCnt = 0;
   }
   else if ((uiVBATValue <  VBAT_LOW_3_45V) && (uiVBAT_3_45V_Pulse == 0))
   {
	   // start short pulse
	   P1OUT |= LOW_BAT;
	   uiVBAT_State = VBAT_STATE_3_45V;
	   uiVBAT_3_45V_Pulse = 1;
	   uiVBAT_3_2V_Pulse = 0;  //if it made it into this else, it means VBAT > 3.2V thus we need to clear pulse flag so we can send pulse again
	   uiVBAT_3_3V_Pulse = 0;  //if it made it into this else, it means VBAT > 3.3V thus we need to clear pulse flag so we can send pulse again
   	   uiIsVBATNotificationActive = 1;
   	   uiVBAT_PulseCnt = 0;
   }
   else if (uiVBATValue >=  VBAT_LOW_3_45V)
   {
	   uiVBAT_3_2V_Pulse = 0;  //if it made it into this else, it means VBAT > 3.2V thus we need to clear pulse flag so we can send pulse again
	   uiVBAT_3_3V_Pulse = 0;  //if it made it into this else, it means VBAT > 3.3V thus we need to clear pulse flag so we can send pulse again
	   uiVBAT_3_45V_Pulse = 0; //if it made it into this else, it means VBAT > 3.45V thus we need to clear pulse flag so we can send pulse again
   }

}

// ADC10 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC10_VECTOR))) ADC10_ISR (void)
#else
#error Compiler not supported!
#endif
{
	uiADC10ISRcalled = 1;
	uiVBATValue = ADC10MEM;
	__bic_SR_register_on_exit(LPM1_bits);
}

// Port 1 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
#else
#error Compiler not supported!
#endif
{

#ifdef LAUNCHPAD_TEST
  uiP1ISRcalled=1;
  P1OUT ^= BIT6;                            // P1.6 = toggle
  P1IFG &= ~BIT3;                           // P1.3 IFG cleared
#else
	if (P1IFG & TP_SW)  			//check if it's a crusher button interrupt
	{
		P1IFG &= ~TP_SW; 			// IFG cleared
		P1IES ^= TP_SW; 			// toggle the interrupt edge,
		uiP1_TP_SW_ISRcalled =1;
	}
	else if (P1IFG & BT_PWR_ON)  	// check if it's a ANC button interrupt
	{
		P1IFG &= ~BT_PWR_ON; 		// IFG cleared
		P1IES ^= BT_PWR_ON; 		// toggle the interrupt edge,
		uiP1_BT_PWR_ON_ISRcalled =1;
	}
	else if (P1IFG & VREG)  		// check if it's a ANC button interrupt
	{
		P1IFG &= ~VREG; 			// IFG cleared
		uiP1_VREG_ISRcalled =1;
	}
#endif
  __bic_SR_register_on_exit(LPM1_bits);
}

// Port 1 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT2_VECTOR))) Port_2 (void)
#else
#error Compiler not supported!
#endif
{

//check which bit triggered ISR and clear that corresponding interrupt flag
#ifdef LAUNCHPAD_TEST
  P1OUT ^= BIT6;                            // P1.6 = toggle
  if (P2IFG & BIT0)
  {
	  P2IFG &= ~BIT0;
	  uiP2ISRcalled=1;
  }
  else if (P2IFG & BIT1)
  {
	  P2IFG &= ~BIT1;
	  uiP2ISRcalled1=1;
  }
  else if (P2IFG & BIT2)
  {
	  P2IFG &= ~BIT2;
	  uiP2ISRcalled2=1;
  }
#else
	if (P2IFG & CR_BUTTON_IN)  		//check if it's a crusher button interrupt
	{
		P2IFG &= ~CR_BUTTON_IN; 	// P1.3 IFG cleared
		P2IES ^= CR_BUTTON_IN; 		// toggle the interrupt edge,
		uiP2_CR_BTN_ISRcalled =1;
	}
	else if (P2IFG & ANC_BTN_IN)  		// check if it's a ANC button interrupt
	{
		P2IFG &= ~ANC_BTN_IN; 		// P1.3 IFG cleared
		P2IES ^= ANC_BTN_IN; 		// toggle the interrupt edge,
		uiP2_ANC_BTN_ISRcalled =1;
	}
#endif

  __bic_SR_register_on_exit(LPM1_bits);
}

/*
// Timer A0 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer_A (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_A0_VECTOR))) Timer_A (void)
#else
#error Compiler not supported!
#endif
{
	// timer interrupt is self-clearing so no need to clear flag
	Citadel_ISRTim1Cnt++;
	uiT1A0ISRcalled = 1;  // set flag so we know there is work to do in main loop.
	TA0CCTL0 &= ~(CCIE);
#ifdef LAUNCHPAD_TEST
	// toggle LED to let us know clock is alive
	P1OUT ^= 0x01;                            // Toggle P1.0
#else
	P3OUT^=BIT7;
#endif


  __bic_SR_register_on_exit(LPM1_bits);
}
*/

// Timer A0 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer_A (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_A0_VECTOR))) Timer_A (void)
#else
#error Compiler not supported!
#endif
{
	uiT1A0ISRcalled = 1;
	P3OUT ^= BIT7;                            // Toggle P1.0
//	P1OUT ^= 0x01;                            // Toggle P1.0
  __bic_SR_register_on_exit(LPM1_bits);
}



